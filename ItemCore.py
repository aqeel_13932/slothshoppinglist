__author__ = 'aqeel'
#Object Represent the Core of Item of User Items
class ItemCore:

    def __init__(self,product):
        #The Product
        self.product = product
        #The stores That Contain The Product
        self.stores =[]
        #Cheapest Stores that Have the Product
        self.finalstores=[]

    #Add Store that have a product
    def AddStore(self,store):
        self.stores.append(store)
    #override to string
    def __str__(self):
        outinfo =str()
        for i in self.stores:
            outinfo+= i.name +' ' +str(i.prices[str(self.product.id)])+'  '

        return self.product.id + '  ' +self.product.Name+'  '+ outinfo

    #another implementation to print the Item
    def GetPrices(self):
        outinfo =str()
        for i in self.finalstores:
            outinfo+= i.name +' ' +str(i.prices[str(self.product.id)])+'  '

        return self.product.Name+'  '+ outinfo

    #Remove Stores that have this item if they are not in the parameter list
    def RemoveStoresNotInList(self,lst):
        self.stores = [s for s in self.stores if s in lst]

    #Find the Cheapest Stores that Has this Item
    def FinalizeStoresPrices(self):
        minprice=600
        for k in range(0,len(self.stores)):
            price = self.stores[k].prices[str(self.product.id)]
            if price==minprice:
                self.finalstores.append(self.stores[k])
                minprice = price

            elif price<minprice:
                del self.finalstores[:]
                self.finalstores.append(self.stores[k])
                minprice = price




