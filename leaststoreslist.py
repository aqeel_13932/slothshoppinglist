import csv
from time import  time
from Product import Product
from Store import Store
from ItemCore import ItemCore
import random
from ShortestPathFinder import ShortestPathFinder
from collections import Counter

#Create Variables
t = time()
#File That Contain prodcuts (Real World Products).
file = open('items.csv')
thereader = csv.DictReader(file)
products = []
stores =[]

#Customer Wanted List
wantedProdcuts = []
CurrentUserHome = Store("Home",random.randint(0,100),random.randint(0,100))
#Function to Preapare the variables
def PrepareVariables ():

    #Store the Products in a List
    print 'Getting Info from CSV File'
    for row in thereader:
        products.append(Product(row['GTIN-14'], row['Name']))

    print 'Creating Stores'
    numberofstores = 16
    for i in range(0,numberofstores):
        stores.append(Store('Store'+str(i),random.randint(0,100),random.randint(0,100)))

    #add Products to the stores
    print 'Adding Products To Stores With Prices'
    for i in range (0,len(products)):
        #at least one store has the customer Product
        storesthathasproduct = random.randint(1,numberofstores-1)
        for l in range(0,storesthathasproduct):
            stores[random.randint(0,numberofstores-1)].AddProduct(products[i])

    #Customer wanted Products
    print 'Creating User List'
    numberofwantedProducts = random.randint(5,25)
    for i in range (0,numberofwantedProducts):
        wantedProdcuts.append(ItemCore(products[random.randint(0,len(products)-1)]))

    #To Find Stores That have the Same Product
    for wp in wantedProdcuts:
        for s in stores:
            if s.HaveProduct(wp.product.id):
                wp.AddStore(s)

'''
The Algorithm in Simple Lines Here :
    #Generate A List of Uniqe Stores For The Products
    #Add Client Home On Top Of The List
    #Calculate The Shortest Path
    #Print The Detials
'''
def FindLeastPriceShortestDitance():
    #Generate A List of Uniqe Stores For The Products
    uniqstores = set()
    for wp in wantedProdcuts:
        wp.FinalizeStoresPrices()
        for s in wp.finalstores:
            uniqstores.add(s.name)


    #Create a list of the unique stores made of stores Objects
    storestomap = filter(lambda x:x.name in uniqstores , stores)

    #Add Home (as a Store Object).
    storestomap.insert(0,CurrentUserHome)

    resut = ShortestPathFinder(storestomap)
    totaldistance,plan = resut.FindApproximateShortestWay()
    print '====== What to Buy , From where , And What Order, Distance :',totaldistance,'======'
    totalcost =0
    for element in plan:
        print element

        items =filter(lambda x: element in x.finalstores , wantedProdcuts)
        for x in items:
            totalcost += element.prices[x.product.id]
            print '   ',x.product.Name , ', Price : ',element.prices[x.product.id]
    print '============ Total Cost : ',totalcost,'==============='
    del resut

'''
The Algorithm in Simple Lines Here :
    #Get An Matrix where each row contain all the stores that have specific product.
    #Generate The Minimum List Stores (Enhanced Greedy Algorithm)
    #Update The Wanted Product Stores.
    #Use Cheapist Shortest Distance Algorithm
'''
def FindLeastStoresShortestDistance():
    t= time()
    #Get An Matrix where each row contain all the stores that have specific product.
    matrix =[]
    for wp in wantedProdcuts:
        lst = []
        for st in wp.stores:
            lst.append(st)
        matrix.append(lst)

    #Generate The Minimum List Stores (Enhanced Greedy Algorithm)
    minimumstores =[]
    while len(matrix)!=0:
        coun = Counter(reduce(lambda x, y: x+y,matrix))
        value = coun.most_common()[0][0]
        matrix = [s for s in matrix if value not in s]
        minimumstores.append(value)

    #Update The Wanted Product Stores.
    print 'Approximate Minimum Stores ',len(minimumstores)
    for wp in wantedProdcuts:
        wp.RemoveStoresNotInList(minimumstores)

    #Use Cheapist Shortest Distance Algorithm
    FindLeastPriceShortestDitance()

    print 'Finding Least Stores,Cheapst, Shortest Distance Finished in ', time()-t


#Make The Variables Ready
PrepareVariables()

#Least Shops -> Shoretest Distance.
print '========Preparing Parameters Finished In : ',time()-t ,'=============='

#Prices -> Shortest Distance
t= time()
FindLeastPriceShortestDitance()
print 'Finding Cheapist Shortest Distance Finished in ', time()-t

#Least Stores -> Least
FindLeastStoresShortestDistance()
