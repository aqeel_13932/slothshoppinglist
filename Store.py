__author__ = 'aqeel'
from Product import Product
import random

#Class Represent the Store
class Store:
    def __init__(self,name,x,y):

        #Store Name
        self.name = name

        #Store location (can be replaced with GPS coordinates in later improvements
        self.x = x
        self.y = y

        #The List of Products that this Store Has
        self.products = {}

        #The Products Prices
        self.prices ={}

    #Add Product to The Store
    def AddProduct (self,product):
        if product.id not in self.products.keys():
            self.products[product.id] = product
            #Generate Random Price for the Product since I don't have a dataset for stores and there prices
            self.prices[product.id] = random.randint(5,500)

    #Check if the store has specific Product or not
    def HaveProduct(self,id):
        return id in self.products.keys()

    #Override to string function for this class
    def __str__(self):
        return self.name